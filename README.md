# Bain Coding Test

- Author: Clint Guerrero
- Email: clintg@gmail.com

## Architecture
For this project I used Flask, a Python web framework, to build out this API
endpoint. I chose Flask because it's a lightweight web framework that 
provides, along with Python, just enough structure to solve this challenge.
This simple application is composed of an endpoint function and a few helper 
functions that live within "app/api.py." 

It's backed by a SQLite database. I chose SQLite because it meets the performance 
needs of this project and is simple to setup. In a production system, with many 
more API clients, I would use a server based SQL DB (e.g. postgresql).

To load data into SQLite, I created a script to import the CSV file into SQLite. 
This script only needs to be run once. Initially I was planning on using SQLite's 
CSV import functionality available from it's Command Line client. However, after 
looking at the data in more detail, I noticed some data that I wanted to clean 
up. This script removes extra space in the header names and more importantly, 
removes $ signs from amounts. Converting charges and payments to numbers allows 
us to more efficiently query numeric ranges.

This code was developed using MacOS. The public version is running on Ubuntu. 
With minimal configuration, this should run on any *NIX OS.

A live version of this code is hosted at:
http://clintg0.pythonanywhere.com/providers

The deployed version of this site requires an "X-API-key" header. You can use curl
to pass this in. See the "Running the server" section below for an example.


## Requirements
- Python 2.7
- Flask
- VirtualEnv / VirtualEnvWrapper: 
 - http://virtualenvwrapper.readthedocs.io/en/latest/install.html#quick-start
- Pip
 - https://pypi.python.org/pypi/pip


## Setting Up
Install virtualenv and/or pip, if they're not yet on your system. This allows
the code to be executed in an environment with all the correct dependencies.
Create a new virtualenv for this project:
`mkvirtualenv bain1`
or use an existing environment (called bain1 in this case):
`workon bain1`

Use pip to install application Requirements:
`pip install -r requirements.txt`

Populate the sqlite database with data from CSV,
Inpatient_Prospective_Payment_System__IPPS__Provider_Summary_for_the_Top_100_Diagnosis-Related_Groups__DRG__-_FY2011.csv
`python csv_to_sqlite.py`
This script creates a new database called IPPS.sqlite at the same level as the 
script.

After this step, the application file/folder structure will look like:
```
.
+-- bain-coding
|   +-- csv_to_sqlite.py 
|   +-- Inpatient_Prospective_Payment_System__IPPS__Provider_Summary_for_the_Top_100_Diagnosis-Related_Groups__DRG__-_FY2011.csv 
|   +-- IPPS.sqlite 
|   +-- README.md
|   +-- requirements.txt
|   +-- test_app.py 
|   +-- app
|       +-- api.py
```

## Running the server

### Development
From the bain-coding dir, you can run the server from a terminal window using:
```FLASK_APP=app/api.py flask run```

To run the server in debug mode. This allows HTTP requests without passing in a 
API Key:
```FLASK_DEBUG=1 FLASK_APP=app/api.py flask run```

### Production
The application is using wsgi, to interface with the Flask app, behind a
simple nginx proxy.

The best way to make requests is using curl. This allows you to pass in an X-API-Key header:
```
curl 'http://clintg0.pythonanywhere.com/providers?min_discharges=15&max_discharges=15&state=WY' -H 'X-API-Key: 94105'
```

## Testing
This code takes advantage of Python's unittest unit testing framework and Flask's 
test_client helpers. Tests can be run from the "bain-coding" directory by running
the following on the command line:
```
python test_app.py
```


### Security

Since this is an internal api, I added a HTTP Header API-Key to ensure there's a  
basic level of security. The key is in the source code config section to make it 
easier to test on another computer. However, in a production setting, this would 
be stored in something like a .env file outside of
the source directory.

Also, in a production environment, there would be SSL encryption to ensure data 
isn't vulnerable over a network connection.