from os.path import dirname, abspath, join
import sqlite3
from flask import Flask, jsonify, g, request, abort
app = Flask(__name__)

app.config.update(dict(
    API_KEY="94105",
    DATABASE=join(dirname(dirname(abspath(__file__))), "IPPS.sqlite")
))


def connect_db():
    """
        Connects to a specified DB
    """
    rv = sqlite3.connect(app.config["DATABASE"])
    rv.row_factory = sqlite3.Row
    return rv

def get_db():
    """ 
        Creates a DB connection 
    """
    if not hasattr(g, "sqlite_db"):
        g.sqlite_db = connect_db()
    return g.sqlite_db

@app.teardown_appcontext
def close_db(error):
    """
        Closes down DB connection after request is completed
    """
    if hasattr(g, "sqlite_db"):
        g.sqlite_db.close()

@app.route("/providers")
def providers():
    """
        Providers Endpoint. Accepts GET URL Params:
        max_discharges (INT), min_discharges (INT), max_average_covered_charges (FLOAT), 
        min_average_covered_charges (FLOAT), max_average_medicare_payments (FLOAT),
        min_average_medicare_payments (FLOAT), state (TWO CHAR STATE ID)
    """
    if app.config["DEBUG"] or ("X-API-Key" in request.headers and request.headers.get("X-API-Key") == app.config['API_KEY']):
        pass
    else:
        abort(401)
        
    app.config["DATABASE"]
    db = get_db()
    query = 'SELECT \
        "Provider Name",\
        "Provider Street Address",\
        "Provider City",\
        "Provider State",\
        "Provider Zip Code",\
        "Hospital Referral Region Description",\
        "Total Discharges",\
        "Average Covered Charges",\
        "Average Total Payments",\
        "Average Medicare Payments"\
        FROM providers \
        WHERE 1=1 '
    if "state" in request.args:
        query += ' AND "Provider State" = "%s"' % request.args.get("state")
    if "max_discharges" in request.args:
        query += ' AND "Total Discharges" <= %s' % request.args.get("max_discharges")
    if "min_discharges" in request.args:
        query += ' AND "Total Discharges" >= %s' % request.args.get("min_discharges")
    if "max_average_covered_charges" in request.args:
        query += ' AND "Average Covered Charges" <= %s' % \
            request.args.get("max_average_covered_charges")
    if "min_average_covered_charges" in request.args:
        query += ' AND "Average Covered Charges" >= %s' % \
            request.args.get("min_average_covered_charges")
    if "max_average_medicare_payments" in request.args:
        query += ' AND "Average Medicare Payments" <= %s' % \
            request.args.get("max_average_medicare_payments")
    if "min_average_medicare_payments" in request.args:
        query += ' AND "Average Medicare Payments" >= %s' % \
            request.args.get("min_average_medicare_payments")
    query += ' ORDER BY "Provider State" ASC, "Total Discharges" ASC, \
        "Average Covered Charges" ASC, "Average Medical Payments" ASC;'
    cur = db.execute(query)
    results = cur.fetchall()
    if results:
        keys = results[0].keys()
        providers_list = []
        # format numeric pricing info to dollar amounts
        for row in results:
            partial = dict(zip(keys,row))
            partial["Average Covered Charges"] = \
                '${0:,.2f}'.format(partial["Average Covered Charges"])
            partial["Average Total Payments"] = \
                '${0:,.2f}'.format(partial["Average Total Payments"])
            partial["Average Medicare Payments"] = \
                '${0:,.2f}'.format(partial["Average Medicare Payments"])
            providers_list.append(partial)
    else:
        providers_list = []
    return jsonify(providers_list)