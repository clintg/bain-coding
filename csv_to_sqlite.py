"""
Desc: Import data into sqlite DB from Inpatient_Prospective_Payment_System__IPPS__Provider_Summary_for_the_Top_100_Diagnosis-Related_Groups__DRG__-_FY2011.csv
Column Names (12): "DRG Definition","Provider Id","Provider Name","Provider Street Address","Provider City","Provider State","Provider Zip Code","Hospital Referral Region Description","Total Discharges","Average Covered Charges","Average Total Payments","Average Medicare Payments"
"""

import csv, sqlite3

con = sqlite3.connect("IPPS.sqlite")
cur = con.cursor()
query_string = 'CREATE TABLE providers ( \
    "DRG Definition",\
    "Provider Id",\
    "Provider Name",\
    "Provider Street Address",\
    "Provider City",\
    "Provider State",\
    "Provider Zip Code",\
    "Hospital Referral Region Description",\
    "Total Discharges" NUMERIC,\
    "Average Covered Charges" NUMERIC,\
    "Average Total Payments" NUMERIC,\
    "Average Medicare Payments" NUMERIC);'
cur.execute(query_string)
cur.execute('CREATE INDEX discharges_idx ON providers("Total Discharges");')
cur.execute('CREATE INDEX average_covered_charges_idx on providers("Average Covered Charges");')
cur.execute('CREATE INDEX average_medicare_payments_idx on providers("Average Total Payments");')
cur.execute('CREATE INDEX state_idx on providers("Average Medicare Payments");')

csv_file = "Inpatient_Prospective_Payment_System__IPPS__Provider_Summary_for_the_Top_100_Diagnosis-Related_Groups__DRG__-_FY2011.csv"
with open(csv_file, 'rb') as f:
    reader = csv.DictReader(f)
    to_db = []
    # Loop over rows. Build a tuple to add to database. Convert $ strings to numeric.
    for item in reader:
        row = (
            item["DRG Definition"],
            item["Provider Id"],
            item["Provider Name"],
            item["Provider Street Address"],
            item["Provider City"],
            item["Provider State"],
            item["Provider Zip Code"],
            item["Hospital Referral Region Description"],
            item[" Total Discharges "], # fix header formatting for this and next 2 fields
            item[" Average Covered Charges "][1:], # remove $ from start of string
            item[" Average Total Payments "][1:], # remove $ from start of string
            item["Average Medicare Payments"][1:] # remove $ from start of string
        )
        to_db.append(row)
    cur.executemany('INSERT INTO providers ("DRG Definition","Provider Id","Provider Name","Provider Street Address","Provider City","Provider State","Provider Zip Code","Hospital Referral Region Description","Total Discharges","Average Covered Charges","Average Total Payments","Average Medicare Payments") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);', to_db)
    con.commit()
    con.close()
