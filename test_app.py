import os, json
import app.api as api
import unittest
import tempfile

class APITestCase(unittest.TestCase):
    def setUp(self):
        """
        Setup testing and get some intial data to use in tests.
        """
        api.app.testing = True

        self.app = api.app.test_client()
        self.api_key_headers = {"X-API-Key":"94105"}
        rv = self.app.get("/providers", headers=self.api_key_headers)
        self.no_filter_results_count = len(json.loads(rv.data))

    def test_base_url(self):
        """
        Ensure no results on main (non /providers) URL
        """
        rv = self.app.get("/", headers=self.api_key_headers)
        assert b"Not Found" in rv.data

    def test_for_any_results(self):
        """
        Ensure you get results for a general query. Reuse setup query data.
        """
        assert(self.no_filter_results_count > 0)

    def test_without_api_key_header(self):
        """
        Ensure we get an error if there are no headers
        """
        rv = self.app.get("/providers?state=WY&max_discharges=27&min_discharges=27")
        assert b"Unauthorized" in rv.data

    def test_correct_field_names(self):
        """
        Ensure field names match those required in API Spec.
        Filtering results on params so we can quickly parse a smaller subset
        """
        rv = self.app.get("/providers?state=WY&max_discharges=15", headers=self.api_key_headers)
        first_provider_result = json.loads(rv.data)[0]
        assert "Provider Name" in first_provider_result
        assert "Provider Street Address" in first_provider_result
        assert "Provider City" in first_provider_result
        assert "Provider State" in first_provider_result
        assert "Provider Zip Code" in first_provider_result
        assert "Hospital Referral Region Description" in first_provider_result
        assert "Total Discharges" in first_provider_result
        assert "Average Covered Charges" in first_provider_result
        assert "Average Total Payments" in first_provider_result
        assert "Average Medicare Payments" in first_provider_result

    def test_max_discharges(self):
        """
        Ensure that we are getting fewer results for the max_discharges param
        """
        rv = self.app.get("/providers?max_discharges=15", headers=self.api_key_headers)
        results = json.loads(rv.data)
        assert len(results) > 0
        assert len(results) < self.no_filter_results_count

    def test_min_discharges(self):
        """
        Ensure that we are getting fewer results for the min_discharges param
        """
        rv = self.app.get("/providers?min_discharges=15", headers=self.api_key_headers)
        results = json.loads(rv.data)
        assert len(results) > 0
        assert len(results) < self.no_filter_results_count

    def test_max_average_covered_charges(self):
        """
        Ensure that we are getting fewer results for the max_average_covered_charges param
        """
        rv = self.app.get("/providers?max_average_covered_charges=5000", headers=self.api_key_headers)
        results = json.loads(rv.data)
        assert len(results) > 0
        assert len(results) < self.no_filter_results_count

    def test_min_average_covered_charges(self):
        """
        Ensure that we are getting fewer results for the min_average_covered_charges param
        """
        rv = self.app.get("/providers?min_average_covered_charges=10000", headers=self.api_key_headers)
        results = json.loads(rv.data)
        assert len(results) > 0
        assert len(results) < self.no_filter_results_count

    def test_max_average_medicare_payments(self):
        """
        Ensure that we are getting fewer results for the max_average_medicare_payments param
        """
        rv = self.app.get("/providers?max_average_medicare_payments=5000", headers=self.api_key_headers)
        results = json.loads(rv.data)
        assert len(results) > 0
        assert len(results) < self.no_filter_results_count

    def test_min_average_medicare_payments(self):
        """
        Ensure that we are getting fewer results for the min_average_medicare_payments param
        """
        rv = self.app.get("/providers?min_average_medicare_payments=10000", headers=self.api_key_headers)
        results = json.loads(rv.data)
        assert len(results) > 0
        assert len(results) < self.no_filter_results_count

    def test_all_params(self):
        """
        Ensure results for URLs with all param filters
        """
        rv = self.app.get("/providers?max_discharges=15&min_discharges=6&max_average_covered_charges=50000&min_average_covered_charges=40000&min_average_medicare_payments=6000&max_average_medicare_payments=10000&state=GA", headers=self.api_key_headers)
        results = json.loads(rv.data)
        assert len(results) > 0
        assert len(results) < self.no_filter_results_count

    def test_state(self):
        """
        Ensure that we are getting fewer results for the state param
        """
        rv = self.app.get("/providers?state=WY", headers=self.api_key_headers)
        results = json.loads(rv.data)
        assert len(results) > 0
        assert len(results) < self.no_filter_results_count

    def test_fake_state(self):
        """
        Ensure that we are getting fewer results for the state param
        """
        rv = self.app.get("/providers?state=MM", headers=self.api_key_headers)
        results = json.loads(rv.data)
        assert not results

    def test_empty_provider_list(self):
        """
        Query a set of params that don't return results. Check for empty list
        """
        rv = self.app.get("/providers?min_discharges=16&max_discharges=15", headers=self.api_key_headers)
        results = json.loads(rv.data)
        assert not results

if __name__ == "__main__":
    unittest.main()
